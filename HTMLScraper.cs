﻿using HtmlAgilityPack;
using System;
using System.Diagnostics;

using System.Text.RegularExpressions;
using System.Xml.Linq;


namespace NewsScraper
{
    /// <summary>
    /// Class for scraping the HTML of a given URI for searchResultItems
    /// </summary>
    public class HTMLScraper
    {
        /// <summary>
        /// Scrape the html of the given URI
        /// </summary>
        /// <param name="uri">The URI to be scraped</param> 
        /// <returns>An XDocument with the scraped information in XML </returns>
        public XDocument ScrapeSite(String uri)
        {
            HtmlWeb hw = new HtmlWeb();
            XDocument scrapeResult = new XDocument();
            XElement news = new XElement("news");
            scrapeResult.Add(news);
            try
            {
                HtmlDocument doc = hw.Load(@uri);
                HtmlNodeCollection coll = doc.DocumentNode.SelectNodes("//div[@class='searchResultItem'] //div[@class='title'] //a[@href]");
                if (coll != null)
                {
                    XElement result = getResultElement(coll, news);
                }

            }
            catch (UriFormatException e)
            {
                if (e.Source != null)
                    Debug.WriteLine("IOException source: {0}", e.Source);
            }
            return scrapeResult;
        }
        /// <summary>
        /// Extracts the information from the collection of nodes, and puts them in XElements.
        /// </summary>
        /// <param name="coll">collection of HTML nodes</param>
        /// <param name="news">the XElement where the Xelements are collected in</param>
        /// <returns>The filled Xelement</returns>
        private XElement getResultElement(HtmlNodeCollection coll, XElement news)
        {

            foreach (HtmlNode link in coll)
            {
                XElement result = new XElement("newsitem");
                HtmlAttribute att = link.Attributes["href"];

                result.Add(
                    new XElement("uri", att.Value.Trim()),
                    new XElement("description", Regex.Replace(link.InnerText.Trim(), @"\t|\n|\r", "")));
                news.Add(result);
            }
            return news;
        }
    }
}
